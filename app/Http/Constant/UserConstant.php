<?php

namespace App\Http\Constant;

class UserConstant {

    const INPUT_ID                   = 'id';
    const INPUT_EMAIL                = 'email';
    const INPUT_PHONE                = 'phone';
    const INPUT_USERNAME             = 'username';
    const INPUT_PASSWORD             = 'password';
    const INPUT_CURRENT_PASSWORD     = 'current_password';
    const INPUT_NEW_PASSWORD         = 'new_password';
    const INPUT_CONFIRM_PASSWORD     = 'confirm_password';
}