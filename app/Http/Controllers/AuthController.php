<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\Http\Constant\UserConstant;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function login( Request $request ) {
        $validator = Validator::make( $request->all(), [
            UserConstant::INPUT_EMAIL    => 'required|string',
            UserConstant::INPUT_PASSWORD => 'required|string|min:6|max:50|alpha_dash'
        ]);
        if ( $validator->fails() ) return response()->json( $validator->errors(), 422 );

        if ( !$token = auth()->attempt( $validator->validated() ) ) return response()->json( [
            'error' => 'Unauthorized'
        ], 401 );

        return $this->createNewToken( $token );
    }

    public function register( Request $request ) {

        $validator = Validator::make( $request->all(), [
            UserConstant::INPUT_USERNAME             => 'required|string|min:6|max:30',
            UserConstant::INPUT_EMAIL                => 'required|email|unique:users,email',
            UserConstant::INPUT_PHONE                => 'size:10|regex:/(0)[1-9]{1}[0-9]{8}/|unique:users,phone',
            UserConstant::INPUT_PASSWORD             => 'required|string|min:6|max:50|alpha_dash',
            UserConstant::INPUT_CONFIRM_PASSWORD     => 'required|string|min:6|max:50|alpha_dash|same:password',
        ] );

        if ( $validator->fails() ) return response()->json( $validator->errors()->toJson(), 400 );
        
        $user = User::create([
            'username'   => $request->username,
            'email'      => $request->email,
            'telephone'  => $request->telephone,
            'status'     => 0,
            'avatar'     => $request->avatar,
            'role'       => 1,
            'password'   => Hash::make( $request->password )
        ]);

        return response()->json([
            'message' => 'User successfully registered',
            'user'    => $user
        ], 201);
    }

    public function logout() {
        auth()->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }

    protected function createNewToken( $token ) {
        return response()->json([
            'access_token'   => $token,
            'token_type'     => 'bearer',
            'expires_in'     => Carbon::now()->addDay(1)->timestamp,
            'user'           => auth()->user()
        ]);
    }
}
