<?php

namespace App\Modules\CategoryMoney\Validators;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\CategoryMoney\Constants\CategoryMoneyConstant;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            CategoryMoneyConstant::INPUT_CATEGORY_MONEY_NAME => 'required',
            CategoryMoneyConstant::INPUT_BALANCE => 'required|int'
        ];
    }

    public function messages()
    {
        return [
            CategoryMoneyConstant::INPUT_CATEGORY_MONEY_NAME .'required' => 'Tên danh mục không được để trống!',
            CategoryMoneyConstant::INPUT_BALANCE .'required' => 'Số tiền không được để trống!',
            CategoryMoneyConstant::INPUT_BALANCE .'int' => 'Số tiền phải là chữ số!',
        ];
    }
}
