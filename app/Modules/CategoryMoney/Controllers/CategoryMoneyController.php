<?php

namespace App\Modules\CategoryMoney\Controllers;

use App\Base\ApiController;
use App\Modules\CategoryMoney\Services\CategoryMoneyService;
use App\Modules\CategoryMoney\Validators\CreateRequest;
use Illuminate\Http\Request;

class CategoryMoneyController extends ApiController {

    protected $catMoneyService;

    public function __construct( CategoryMoneyService $catMoneyService )
    {
        $this->catMoneyService = $catMoneyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->catMoneyService->getList();
        return $this->responseSuccess($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $result = $this->catMoneyService->create($request->all());
        if ($result) {
            return $this->responseSuccess($result);
        }
        return $this->responseError('Không thành công.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = $this->catMoneyService->getOne($id);
        return $this->responseSuccess($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = $this->catMoneyService->update($id, $request->all());
        if ($result) {
            return $this->responseSuccess($result);
        };
        return $this->responseError('Không thành công.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if ($this->catMoneyService->delete($id)) {
            return $this->responseSuccess();
        }

        return $this->responseError('Không thành công');
    }

}