<?php

namespace App\Modules\CategoryMoney\Services;

use App\Repositories\CatMoneyRepository;
use Illuminate\Support\Facades\Auth;

class CategoryMoneyService {

    protected $catMoneyRepository;

    public function __construct( CatMoneyRepository $catMoneyRepository )
    {
        $this->catMoneyRepository = $catMoneyRepository;
    }

    public function getList() {
        $query = $this->catMoneyRepository->query()->orderBy('id', 'desc');
        return $this->catMoneyRepository->getAll(['*'], $query, config('setting.paginate'));
    }

    public function getOne($id) {
        return $this->catMoneyRepository->findOrFail($id);
    }

    public function create($params) {
        // $dataInsert['created_by'] = Auth::id();
        $params['created_by'] = 1;

        return $this->catMoneyRepository->create($params);
    }

    public function update($id, $params) {
        return $this->catMoneyRepository->update($id, $params);
    }

    public function delete($id) {
        return $this->catMoneyRepository->deleteItem($id);
    }
    



}