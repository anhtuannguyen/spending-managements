<?php

namespace App\Modules\CategoryMoney\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryMoney extends Model {
    
    protected $table       = 'category_money';
    protected $guarded   = [];
}