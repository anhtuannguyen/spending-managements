<?php

namespace App\Modules\CategoryMoney\Constants;

class CategoryMoneyConstant {
    const INPUT_CATEGORY_MONEY_NAME  = 'cat_money_name';
    const INPUT_STATUS               = 'status';
    const INPUT_BALANCE              = 'balance';
    const INPUT_COLOR                = 'color';
    const INPUT_ICON                 = 'icon';
    const INPUT_DESCRIPTION          = 'description';
    const INPUT_CREATED_BY           = 'created_by';
}