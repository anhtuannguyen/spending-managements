<?php

namespace App\Modules\Spending\Controllers;

use App\Base\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Spending\Services\SpendingService;

class SpendingController extends ApiController
{
    protected $spendingService;

    public function __construct(SpendingService $spendingService)
    {
        $this->spendingService = $spendingService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spendings = $this->spendingService->getList();
        return $this->responseSuccess($spendings);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = $this->spendingService->create($request->all());
        if ($result) {
            return $this->responseSuccess($result);
        }
        return $this->responseError('Không thành công.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = $this->spendingService->getOne($id);
        return $this->responseSuccess($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = $this->spendingService->update($id, $request->all());
        if ($result) {
            return $this->responseSuccess($result);
        }
        return $this->responseError('Không thành công.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if ($this->spendingService->delete($id)) {
            return $this->responseSuccess();
        }

        return $this->responseError('Không thành công');
    }
}
