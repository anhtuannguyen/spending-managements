<?php

namespace App\Modules\Spending\Services;
use App\Repositories\SpendingRepository;

class SpendingService {

    protected $spendingRepository;

    public function __construct(SpendingRepository $spendingRepository)
    {
        $this->spendingRepository = $spendingRepository;
    }

    public function getList() {
        $query = $this->spendingRepository->query()->orderBy('id', 'desc');
        return $this->spendingRepository->getAll(['*'], $query, config('setting.paginate'));
    }

    public function getOne($id) {
        return $this->spendingRepository->findOrFail($id);
    }

    public function create($params) {
        $params['create_by'] = 1;
        return $this->spendingRepository->create($params);
    }

    public function update($id, $params) {
        return $this->spendingRepository->update($id, $params);
    }

    public function delete($id) {
        return $this->spendingRepository->deleteItem($id);
    }
    
}