<?php

namespace App\Modules\Spending\Constants;

class SpendingConstant {
    const INPUT_NOTE          = 'note';
    const INPUT_NOTE_DATE     = 'note_date';
    const INPUT_DESCRIPTION   = 'description';
    const INPUT_IMAGE_PATH    = 'image_path';
    const INPUT_MENEY         = 'meney';
    const INPUT_CAT_MONEY_ID  = 'cat_money_id';
    const INPUT_STATUS        = 'status';
    const INPUT_CREATE_BY     = 'created_by';
}