<?php

namespace App\Modules\Spending\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpendingModel extends Model
{
    protected $table       = 'spending';
    protected $guarded   = [];
}
