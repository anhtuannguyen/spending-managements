<?php

namespace App\Repositories;

use App\Modules\CategoryMoney\Models\CategoryMoney;
use Illuminate\Support\Facades\DB;

class CatMoneyRepository extends BaseRepository {

    public function __construct(CategoryMoney $categoryMoney)
    {
        $this->model = $categoryMoney;
    }

    public function deleteItem($id) {
        DB::beginTransaction();
        try {
            $this->model->where('parent_id', $id)->delete();
            $this->model->where('id', $id)->delete();
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e);
            return false;
        }
    }

    

    // public function paginate( $limit = 15 ) {
    //     return CategoryMoney::paginate( $limit );
    // }

}