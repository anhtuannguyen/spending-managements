<?php

namespace App\Repositories;

use App\Modules\Spending\Models\SpendingModel;
use Illuminate\Support\Facades\DB;

class SpendingRepository extends BaseRepository {

    public function __construct(SpendingModel $spendingModel)
    {
        $this->model = $spendingModel;
    }

    public function deleteItem($id) {
        DB::beginTransaction();
        try {
            $this->model->where('id', $id)->delete();
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger()->error($e);
            return false;
        }
    }

}