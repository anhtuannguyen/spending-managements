<?php

namespace App\Repositories;

class BaseRepository {

    protected $model;

    public function query() {
        return $this->model;
    }

    public function getAll($select = ['*'], $queryInput = null, $paginate = null) {

        if (!empty($queryInput)) {
            $query = $queryInput->select($select);
        } else {
            $query = $this->model->select($select);
        }

        return $paginate ? $query->paginate($paginate) : $query->get();

    }

    public function create($params) {
        return $this->model->create($params);
    }

    public function findOrFail($id) {
        return $this->model->findOrFail($id);
    }

    public function update($id, $params) {

        $model = $this->model->find($id);
        $model->update($params);

        return $this->model->find($id);
    }
}