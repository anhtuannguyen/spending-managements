<?php

namespace App\Base;

class ResponseConstant {


    const HTTP_OK = 200;
    const STATUS  = 'status';
    const CODE    = 'code';
    const MESSAGE = 'message';
    const DATA    = 'data';
    const SUCCESS = 1;
    const FAIL    = 0;
    const MESSAGE_OK = 'OK';
    const UNKNOWN_FAIL = -1;
    const UNDEFINED_EXCEPTION = 'UNDEFINED_EXCEPTION';
    const UNAUTHORIZED = 'UNAUTHORIZED';
    const FORBIDDEN = 'FORBIDDEN';
    const KEY_TOKEN = 'token';
}