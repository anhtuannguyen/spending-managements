<?php

namespace App\Base;

use Illuminate\Contracts\Validation\Validator as ValidatorInterface;

abstract class Validator implements ValidatorInterface {

    protected $input = [];

    /**
     * The validator implementation
     * 
     * @var ValidatorInterface
     */
    protected ValidatorInterface $validator;

    public function __construct($input = [])
    {
        $this->input = $input;
        $this->validator = $this->makeValidator($this->input);
        $this->validator->after(function () {
            if ($this->errors()) {

            }
        });
        // print_r($this->validator);
    }

    public function makeValidator(array $input) {
        return validator($input, $this->rules(), $this->messages(), $this->labels());
    }

    public function rules() {
        return [];
    }

    public function messages() {
        return [];
    }

    public function labels() {
        return [];
    }

    public function validate() {
        return $this->validator->validate();
    }

    public function validated() {
        return $this->validator->validated();
    }

    public function fails()
    {
        return $this->validator->fails();
    }

    public function failed() {
        return $this->validator->failed();
    }

    /**
     * Add conditions to a given field based to on Closure.
     * 
     * @param string|array $attribute
     * @param string|array $reles
     * @param callable $callback
     * @return ValidatorInterface
     */
    public function sometimes($attribute, $rules, callable $callback)
    {
        return $this->validator->sometimes($attribute, $rules, $callback);
    }

    /**
     * After an after validation callback.
     *
     * @param callable|string $callback
     * @return ValidatorInterface
     */
    public function after($callback)
    {
        return $this->validator->after($callback);
    }

    /**
     * Get all of the validation error messages.
     *
     * @return MessageBag
     */
    public function errors()
    {
        return $this->validator->errors();
    }

    /**
     * Get the messages for the instance.
     *
     * @return \Illuminate\Contracts\Support\MessageBag
     */
    public function getMessageBag()
    {
        return $this->validator->getMessageBag();
    }
}