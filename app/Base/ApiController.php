<?php

namespace App\Base;

use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController {
 
    /**
     * Make the custom response
     * @param integer|$code
     * @param string $message
     * @param mixed|array $data
     * @return JsonResponse
     */
    public function response($code = 1, $message = 'OK', $data = []) {
        return response()->json([
            ResponseConstant::CODE       => $code,
            ResponseConstant::MESSAGE    => $message,
            ResponseConstant::DATA       => $data
        ], ResponseConstant::HTTP_OK);
    }

    /**
     * Make the success response
     * 
     * @param mixed|array $data
     * @return JsonResponse
     */
    public function responseSuccess($data = []) {
        return response()->json([
            ResponseConstant::CODE       => ResponseConstant::HTTP_OK,
            ResponseConstant::MESSAGE    => ResponseConstant::MESSAGE_OK,
            ResponseConstant::DATA       => $data
        ], ResponseConstant::HTTP_OK);
    }

    /**
     * Make the error response
     * @param string $message
     * @param mixed|array $data
     * @return JsonResponse
     */
    public function responseError($message, $data = []) {
        return response()->json([
            ResponseConstant::CODE       => ResponseConstant::FAIL,
            ResponseConstant::MESSAGE    => $message,
            ResponseConstant::DATA       => $data
        ], ResponseConstant::HTTP_OK);
    }
}