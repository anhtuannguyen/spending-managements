<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spending', function (Blueprint $table) {
            $table->id();
            $table->string('note', 100);
            $table->dateTime('note_date');
            $table->text('description');
            $table->integer('meney');
            $table->foreignId('cat_money_id')->constrained('category_money')->onUpdate('cascade')->onDelete('cascade');
            $table->tinyInteger('status')->default(0); // 0 la so tien chi va 1 la so tien thu
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spending');
    }
};
