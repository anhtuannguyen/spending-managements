<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_money', function (Blueprint $table) {
            $table->id();
            $table->string('cat_money_name', 100);
            $table->tinyInteger('status')->default(0);
            $table->integer('balance');
            $table->string('color', 11);
            $table->string('icon', 11);
            $table->text('description');
            $table->foreignId('created_by')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_money');
    }
};
