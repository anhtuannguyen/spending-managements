<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::group([
        'middleware' => 'api',
        'prefix' => 'auth',
        'namespace' => 'App\Http\Controllers'
    ], function () {
        Route::post('/login', 'AuthController@login');
        Route::post('/register', 'AuthController@register');
        Route::post('/logout', 'AuthController@logout');
    });

    Route::group([
        'prefix' => 'cat-money',
        'namespace' => 'App\Modules\CategoryMoney'
    ], function () {
        Route::get('/', 'Controllers\CategoryMoneyController@index');
        Route::post('/create', 'Controllers\CategoryMoneyController@store');
        Route::post('/update/{id}', 'Controllers\CategoryMoneyController@update');
        Route::get('/detail/{id}', 'Controllers\CategoryMoneyController@show');
        Route::post('/delete/{id}', 'Controllers\CategoryMoneyController@delete');

    });

    Route::group([
        'prefix' => 'spending',
        'namespace' => 'App\Modules\Spending'
    ], function () {
        Route::get('/', 'Controllers\SpendingController@index');
        Route::post('/create', 'Controllers\SpendingController@store');
        Route::post('/update/{id}', 'Controllers\SpendingController@update');
        Route::get('/detail/{id}', 'Controllers\SpendingController@show');
        Route::post('/delete/{id}', 'Controllers\SpendingController@delete');
    });

    
} );
